from django.test import TestCase

class FirstAppTestCase(TestCase):
    def test_simple(self):
        self.assertEqual(1,[1,2,3].index(2))
        with self.assertEqual(ValueError):
            [1,2,3].index(4)